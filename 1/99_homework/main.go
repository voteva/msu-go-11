package main

import (
    "strconv"
    "sort"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(sl []int) string {
	var result string
	for _, value := range sl {
		result += strconv.Itoa(value)
	}
	return result;
}

func MergeSlices(sl1 []float32, sl2 []int32) []int {
	var intSlice []int
	for _, val := range sl1 {
		intSlice = append(intSlice, int(val))
	}
	for _, val := range sl2 {
		intSlice = append(intSlice, int(val))
	}	
	return intSlice
}

func GetMapValuesSortedByKey(input map[int]string) []string {		
	var result []string
	var keys []int
	for key, _ := range input {
		keys = append(keys, key)
	}	
	sort.Ints(keys)	
	for _, key := range keys {
		result = append(result, input[key])
	}
	return result
}

